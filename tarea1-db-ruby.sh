#!/bin/bash
export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=mysecretpassword
export POSTGRESIP=$(docker-machine ip machine1)

docker run --name some-postgres -p 5432:5432 -e POSTGRES_USER=$POSTGRES_USER -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -d postgres

docker run -it -e POSTGRESIP=$POSTGRESIP -e POSTGRES_USER=$POSTGRES_USER -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD  -p 3000:3000 --name some-ruby --link some-postgres:db  mayrez/sample_app_image:0.5 /bin/bash -c \
        'rake db:setup  &&
        rake db:migrate &&
        rake db:populate &&
       rails server'

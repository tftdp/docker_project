#!/bin/bash

export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=mysecretpassword
export AWS_ACCESS_KEY_ID=$aws_access_key_id
export AWS_SECRET_ACCESS_KEY=$aws_secret_access_key
export AWS_VPC_ID=vpc-835dbbfa
export AWS_DEFAULT_REGION=us-east-1
export AWS_ZONE=c

export AWS_SECURITY_GROUP=Consul-Postgres
docker-machine -D create --driver amazonec2 \
  --amazonec2-access-key $AWS_ACCESS_KEY_ID \
  --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
  --amazonec2-vpc-id $AWS_VPC_ID \
  --amazonec2-security-group $AWS_SECURITY_GROUP \
  --amazonec2-region $AWS_DEFAULT_REGION \
  --amazonec2-zone $AWS_ZONE \
postgres-machine

eval $(docker-machine env postgres-machine)
export CONSULIP=$(docker-machine ip postgres-machine)
export POSTGRES=$(docker-machine ip postgres-machine)
docker-compose -f docker-compose-tarea4-postgres.yml up &

until PGPASSWORD="$POSTGRES_PASSWORD" psql -h "$POSTGRES" -U "$POSTGRES_USER" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done &&
echo "Postgres available"

export AWS_SECURITY_GROUP=Ruby
docker-machine -D create --driver amazonec2 \
  --amazonec2-access-key $AWS_ACCESS_KEY_ID \
  --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
  --amazonec2-vpc-id $AWS_VPC_ID \
  --amazonec2-security-group $AWS_SECURITY_GROUP \
  --amazonec2-region $AWS_DEFAULT_REGION \
  --amazonec2-zone $AWS_ZONE  \
  --engine-opt="cluster-store=consul://$(docker-machine ip postgres-machine):8500" \
  --engine-opt="cluster-advertise=eth0:2376" \
ruby-machine

export AWS_SECURITY_GROUP=Nginx
docker-machine -D create --driver amazonec2 \
  --amazonec2-access-key $AWS_ACCESS_KEY_ID \
  --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
  --amazonec2-vpc-id $AWS_VPC_ID \
  --amazonec2-security-group $AWS_SECURITY_GROUP \
  --amazonec2-region $AWS_DEFAULT_REGION \
  --amazonec2-zone $AWS_ZONE  \
  --engine-opt="cluster-store=consul://$(docker-machine ip postgres-machine):8500" \
  --engine-opt="cluster-advertise=eth0:2376" \
nginx-machine


eval $(docker-machine env ruby-machine)
export NODE2=$(docker-machine ip ruby-machine)
docker network create -d overlay --subnet=10.0.9.0/24 multi-host-net
docker-compose -f docker-compose-tarea4-ruby.yml up &

until  wget $(docker-machine ip ruby-machine):3000/public -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Ruby server is not running - sleeping"
  sleep 1
done &&

eval $(docker-machine env nginx-machine)
export NODE3=$(docker-machine ip nginx-machine)
docker-compose -f docker-compose-tarea4-nginx.yml up &

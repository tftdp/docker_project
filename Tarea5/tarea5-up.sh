#!/bin/bash

export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=mysecretpassword

docker-machine create -d virtualbox consul-machine
docker-machine regenerate-certs consul-machine -f
eval $(docker-machine env consul-machine)
export CONSULIP=$(docker-machine ip consul-machine)
docker-compose -f docker-compose-tarea5-consul.yml up &

until  wget $(docker-machine ip consul-machine):8500 -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Consul server is not running - sleeping"
  sleep 1
done &&
echo "Consul server available"

docker-machine create -d virtualbox  \
--swarm --swarm-master \
--swarm-discovery="consul://$(docker-machine ip consul-machine):8500" \
--engine-opt="cluster-store=consul://$(docker-machine ip consul-machine):8500" \
--engine-opt="cluster-advertise=eth1:2376" \
nginx-machine

docker-machine regenerate-certs nginx-machine -f

docker-machine create -d virtualbox  \
--swarm \
--swarm-discovery="consul://$(docker-machine ip consul-machine):8500" \
--engine-opt="cluster-store=consul://$(docker-machine ip consul-machine):8500" \
--engine-opt="cluster-advertise=eth1:2376" \
ruby-machine

docker-machine regenerate-certs ruby-machine -f

docker-machine create -d virtualbox  \
--swarm  \
--swarm-discovery="consul://$(docker-machine ip consul-machine):8500" \
--engine-opt="cluster-store=consul://$(docker-machine ip consul-machine):8500" \
--engine-opt="cluster-advertise=eth1:2376" \
postgres-machine

docker-machine regenerate-certs postgres-machine -f

export MANAGER=$(docker-machine ip nginx-machine)
export NODE2=$(docker-machine ip postgres-machine)
export NODE3=$(docker-machine ip ruby-machine)

docker-machine ssh nginx-machine sudo docker network create -d overlay --subnet=10.0.9.0/24 multi-host-net

eval $(docker-machine env nginx-machine)
docker run -d --name=registrator -h $(docker-machine ip nginx-machine) -v=/var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator:v6 consul://$(docker-machine ip consul-machine):8500
eval $(docker-machine env postgres-machine)
docker run -d --name=registrator -h $(docker-machine ip postgres-machine) -v=/var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator:v6 consul://$(docker-machine ip consul-machine):8500
eval $(docker-machine env ruby-machine)
docker run -d --name=registrator -h $(docker-machine ip ruby-machine) -v=/var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator:v6 consul://$(docker-machine ip consul-machine):8500

eval $(docker-machine env postgres-machine)
export POSTGRES=$(docker-machine ip postgres-machine)
docker-compose -f docker-compose-tarea5-postgres.yml up &

until PGPASSWORD="$POSTGRES_PASSWORD" psql -h "$POSTGRES" -U "$POSTGRES_USER" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done &&

eval $(docker-machine env ruby-machine)
docker-compose -f docker-compose-tarea5-ruby.yml up &

until  wget $(docker-machine ip ruby-machine):3000/public -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Ruby server is not running - sleeping"
  sleep 1
done &&

eval $(docker-machine env nginx-machine)
docker-compose -f docker-compose-tarea5-nginx.yml up &

until  wget $(docker-machine ip nginx-machine):8080/public -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Cannot access application from Nginx server"
  sleep 1
done &&

echo "App available from Nginx server"

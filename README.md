
Despliegue automático de una aplicación web en un cluster Swam utilizando los servicios en la nube de Amazon Web Services (AWS) y tecnología Docker
===================================================================================================================================================

+ Se incluyen los scripts y ficheros de descripción (*.yml) necesarios para el despliegue automático de una aplicación web localmente y en la nube de Amazon Web Services(AWS)
mediante la ejecución de dichos scripts en un sistema Linux. El despliegue será válido para entornos de test y desarrollo, pero no para un entorno de producción.
+ Se define una infraestructura que sirva como entrada a la herramienta docker-compose mediante un fichero (*.yml). docker-compose se utiliza también para provisionar un
host con el servicio EC2 de AWS donde se ejecutarán los contenedores correspondientes de los servicios virtualizados.
+ De esta manera se obtiene una aplicación web de 3 capas que puede ser desplegada localmente utilizando la herramienta docker-compose y un host desplegado con VirtualBox 
o el servicio EC2 de AWS.
+ Los ficheros de script se adaptarán para cada tarea en función de la forma en la que se vaya a lanzar (sólo con Consul o en un cluster Swarm, en local o en AWS). 
+ Los ficheros (*.yml) para la herramienta docker-compose se modifican para tener en cuenta que los servicios funcionan en diferentes hosts y resolver el problema del
 descubrimiento mediante la tecnología Consul y mediante la definición de redes virtuales (network overlay).

# Prerrequisitos

- [VirtualBox](https://www.virtualbox.org/) (versión 4.3)
- [Docker](https://www.docker.com/) (versión 1.12.5). La guía de instalación de Docker está en https://docs.docker.com/engine/installation/
- [Docker Machine](https://docs.docker.com/machine/) (versión 0.9.0-rc2). La guía de instalación de Docker Machine está en https://docs.docker.com/machine/install-machine/
- [Docker Compose](https://docs.docker.com/compose/) (versión 1.9.0). La guía de instalación de Docker Compose se encuentra en https://docs.docker.com/compose/install


Pasos Previos: Creación de las imágenes Docker de Ruby y del servidor web Nginx
----------------------------------------------------------------------------------------------------------------------------------------------------------

### Creación de imagen Docker de la aplicación de Ruby
Se creará una imagen Docker a partir del dicrectorio del proyecto sample_app y luego esta imagen será añadida al repositorio de Docker.
Para ello, se creará un fichero Dockerfile a partir del cual será creada dicha imagen. En este fichero será indicado la imagen a partir de la cual será creada. En este caso a partir de una imagen Ruby ya que para que la aplicación pueda ejecutarse necesita tenerlo instalado. También se indicará qué comandos adicionales deben ejecutarse.
Se creará el tag del contenedor a partir del id de la imagen creada que se puede ver al ejecutar 'docker images'.
Finalmente, se inicia sesión en Docker y se realiza un push de la imagen al repositorio.
+ A continuación, se muestra el contenido del Dockerfile para la creación de la imagen de la aplicación de Ruby:

```console
FROM ruby:2.2.6-onbuild

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]

RUN apt-get update \
  && apt-get -y install nodejs  && rm -rf /var/lib/apt/lists/* \
RUN cp config/database.yml.postgres config/database.yml

COPY Gemfile /usr/src/app


RUN bundle install

COPY . /usr/src/app
```
+ A continuación se describen los pasos para crear la imagen de la aplicación.
```console
$ git clone https://github.com/mayrez/sample_app_rails_4.git
$ cd sample_app_rails_4
$ docker build -t sample_app_image .
$ docker tag 7d9495d03763 mayrez/sample_app_image:0.5
$ docker login
$ docker push mayrez/sample_app_image:0.5
```


### Creación de imagen Docker de Nginx
+ Para ello se creará un fichero Dockerfile a partir del cual será creada la imagen Docker. En este fichero se indicará a partir de qué imágen la crearemos.
+ Se creará el tag del contenedor a partir del id de la imagen creada que se puede ver al ejecutar 'docker images'
+ Finalmente, se inicia sesión en Docker y se realiza un push de la imagen al repositorio.
+ A continuación se describe el contenido del Dockerfile de Nginx: 
```console
FROM nginx
COPY ./etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf
```
+ Los pasos para crear la imagen de la aplicación son los siguientes:
```console
$ git clone https://bitbucket.org/tftdp/docker_project
$ cd nginx
$ docker build -t some-nginx .
$ docker images
$ docker tag 7d9495d03763 mayrez/some-nginx:0.1
$ docker login
$ docker push mayrez/some-nginx:0.1
```

1: Adaptación de la aplicación web de 3 capas. Despliegue local de la aplicación en una única máquina mediante contenedores lanzados desde un shell.
----------------------------------------------------------------------------------------------------------------------------------------------------------

En este paso se realizará el despliegue de la aplicación sample_app con 3 comandos “docker run ...”: uno para el servidor web, otro para el servidor de aplicaciones y otro para la base de datos.
Antes de comenzar con el paso 1 se habrá creado las imágenes Docker de la aplicación Ruby y Nginx, siguiendo los pasos descritos en los apartados anteriores.
En primer lugar se crea el host desde el que se realizará el despliegue de la aplicación 
Para el despliegue de la base de datos y la aplicación se ejecuta el script tarea1-db-ruby.sh.
```console
#!/bin/bash
export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=mysecretpassword
export POSTGRESIP=$(docker-machine ip machine1)

docker run --name some-postgres -p 5432:5432 -e POSTGRES_USER=$POSTGRES_USER -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -d postgres

docker run -it -e POSTGRESIP=$POSTGRESIP -e POSTGRES_USER=$POSTGRES_USER -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD  -p 3000:3000 --name some-ruby --link some-postgres:db  mayrez/sample_app_image:0.5 /bin/bash -c \
        'rake db:setup  &&
        rake db:migrate &&
        rake db:populate &&
        rails server'
```
Por último, se lanzará el contenedor de some-nginx con el fichero tarea1-nginx.sh 
```console
#!/bin/bash
docker run --name some-nginx --link some-ruby:app -p 8080:80 -d mayrez/some-nginx:0.1
```
Para comprobar el correcto funcionamiento de la base de datos ejecutaremos el rails console y se realizarán operaciones con la misma. Para obtener el id del contenedor de ruby y poder acceder a su consola, ejecutamos 'docker ps' para obtener el id del contenedor. Una vez obtenido el id del contenedor ejecutaremos 'docker exec -it id bash' y accederemos a la consola del mismo.

+ Los pasos a seguir son los siguientes:
```console
$ docker-machine create --virtualbox-disk-size "32768"  -d virtualbox machine1
$ eval $(docker-machine env machine1)
$ sh tarea1-db-ruby.sh
```
+ Ahora ejecutamos desde otro shell los siguientes comandos:
```console
$ eval $(docker-machine env machine1)
$ sh tarea1-nginx.sh
$ curl $(docker-machine ip machine1):8080/public
$ docker ps
$ docker exec -it ed16a03dee8d bash
root@ed16a03dee8d:/usr/src/app# rails console
Loading development environment (Rails 4.0.8)
irb(main):001:0> User.first
  User Load (1.0ms)  SELECT "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT 1
=> #<User id: 1, name: "Example User", email: "example@railstutorial.org", created_at: "2017-04-24 16:12:45", updated_at: "2017-04-24 16:12:45", password_digest: "$2a$10$jshXYIy5vQNagWEJcsSMfe23y.oSrvXv.w/RwEyfc89V...", remember_token: "ac60197bf21ccf2b2b60a3d4136a8db94977c24e", admin: true>
```

También se puede comprobar el funcionamiento de la aplicación en el navegador del host accediendo a tráves de la ip de la máquina machine1



2: Despliegue local de la aplicación mediante un fichero de descripción utilizando la herramienta docker-compose. 
-----------------------------------------------------------------------------------------------------------------------

En este paso se realizará el despliegue de la aplicación en único host con un único fichero docker-compose.yml (docker-compose-tarea2.yml). En este fichero se describen los tres servicios que componen la aplicación (Postgres, aplicación Ruby y Nginx).
Antes de comenzar con el paso 2 se habrá creado las imágenes Docker de la aplicación Ruby y Nginx, siguiendo los pasos descritos en los apartados anteriores.

+ El contenido del fichero 'docker-compose-tarea2.yml' es el siguiente:

```console
version: '2'

services:
  some-postgres:
    build: postgres
    container_name: postgres
    ports:
      - 5432:5432
    environment:
      - POSTGRES_USER=${POSTGRES_USER}
      - POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
    restart: always

  some-ruby:
    image: mayrez/sample_app_image:0.5
    ports:
      - 3000:3000
    links:
      - some-postgres:db
    working_dir: /usr/src/app
    environment:
      - POSTGRES_USER=$POSTGRES_USER
      - POSTGRES_PASSWORD=$POSTGRES_PASSWORD
      - POSTGRESIP=$POSTGRES
      - WEB_CONCURRENCY=1 # How many worker processes to run
      - RAILS_MAX_THREADS=16 # Configure to be the maximum number of threads
    restart: always
    command:
      /bin/bash -c \
        'cp config/database.yml.postgres config/database.yml && 
        rake db:setup &&
        rake db:migrate &&
        rake db:populate &&
        rails server'
    depends_on:
      - some-postgres

  some-nginx:
    image: mayrez/some-nginx:0.1  
    ports:
      - 8080:80
    links:
      - some-ruby:app
    restart: always
```

El servicio Postgres se construye a partir del directorio Postgres del proyecto. Este contiene un fichero Dockerfile que indica de donde obtener la imagen. 
El servicio de Ruby depende del contenedor de Postgres. Se le pasan las variables de configuración de la base de datos y los comandos a ejectura para configurar la base de datos de la aplicación.
Por último, se crea el servicio de Nginx que enlaza con la aplicación de Ruby.
Para comprobar el correcto funcionamiento de la base de datos ejecutaremos el rails console y se realizarán operaciones con la misma. Para obtener el id del contenedor de Ruby y poder acceder a su consola, ejecutamos 'docker ps' para obtener el id del contenedor. Una vez obtenido el id del contenedor ejecutaremos 'docker exec -it id bash' y accederemos a la consola del mismo.

+ A continuación, se muestran los comandos para el despliegue de la aplicación y la comprobación de su funcionamiento:

```console
$ mkdir postgres
$ echo 'FROM postgres' >> postgres/Dockerfile
$ echo 'MAINTAINER M. García <mayrezbritto@gmail.com>' >> postgres/Dockerfile
$ docker-compose -f docker-compose-tarea2.yml up
$ curl $(docker-machine ip machine1):8080/public
$ docker ps
$ docker exec -it ed16a03dee8d bash
root@ed16a03dee8d:/usr/src/app# rails console
Loading development environment (Rails 4.0.8)
irb(main):001:0> User.first
  User Load (1.0ms)  SELECT "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT 1
=> #<User id: 1, name: "Example User", email: "example@railstutorial.org", created_at: "2017-04-24 16:12:45", updated_at: "2017-04-24 16:12:45", password_digest: "$2a$10$jshXYIy5vQNagWEJcsSMfe23y.oSrvXv.w/RwEyfc89V...", remember_token: "ac60197bf21ccf2b2b60a3d4136a8db94977c24e", admin: true>
```
También se puede comprobar en el navegador del host accediendo a tráves de la ip de la máquina machine1.


##3: Adaptación de la configuración para que cada una de las capas de la aplicación funcione en tres MV diferentes (una para el servicio de base de datos, otra para el servidor web y otra para el  servidor de aplicaciones) con docker-machine.

El fichero docker-compose.yml que antes era único será descompuesto en tres ficheros docker-compose.yml distintos para desplegar cada uno de los servicios en un host distinto. Los links que se tenía antes y funcionaban bien dejarán de funcionar ya que ahora cada servicio funciona en una máquina virtual diferente que no tiene conexión con las otras. 
Para arreglar este problema del descubrimiento de los servicios se usará Consul y overlay networking:

   + El Overlay networking permite que  un contenedor se conecte a otro contenedor usando una IP local. Este tipo de networking requiere de almacenamiento de key-value. En este caso se usará Consul como key-value store.
   + Consul permite que un contenedor se pueda conectar a otro usando el nombre. 

Se desplegará cada una de las capas de la aplicación en una máquina distinta ejecutando el script tarea3.sh, que incluyen comandos docker-machine  y docker-compose. Tendremos 3 Máquinas virtuales: postgres-machine (con el servidor Consul y la base de datos), ruby-machine (con un agente Consul que conecta con el servidor Consul y la aplicación de Ruby) y nginx-machine (con otro agente Consul que se conecta al servidor de Consul y el servidor web). 
 Para la creación del key-value store y la base de datos se ejecutará el fichero docker-compose-tarea3-postgres.yml, para la creación de la capa de la aplicación de Ruby se ejecutará el fichero docker-compose-tarea3-ruby.yml y para la creación del servicio web se ejecutará el fichero docker-compose-tarea3-nginx.yml.
+ El contenido del script 'Tarea3/tarea3-up.sh' a ejecutar para levanatar la infraestructura es el siguiente:
```console 
#!/bin/bash

export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=mysecretpassword

docker-machine create -d virtualbox postgres-machine
docker-machine regenerate-certs postgres-machine -f
eval $(docker-machine env postgres-machine)
export CONSULIP=$(docker-machine ip postgres-machine)
export POSTGRES=$(docker-machine ip postgres-machine)
docker-compose -f docker-compose-tarea3-postgres.yml up &

until PGPASSWORD="$POSTGRES_PASSWORD" psql -h "$POSTGRES" -U "$POSTGRES_USER" -c '\l'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done &&

docker-machine create -d virtualbox  \
--engine-opt="cluster-store=consul://$(docker-machine ip postgres-machine):8500" \
--engine-opt="cluster-advertise=eth1:2376" \
ruby-machine

docker-machine regenerate-certs ruby-machine -f

docker-machine create -d virtualbox  \
--engine-opt="cluster-store=consul://$(docker-machine ip postgres-machine):8500" \
--engine-opt="cluster-advertise=eth1:2376" \
nginx-machine

docker-machine regenerate-certs nginx-machine -f

export NODE2=$(docker-machine ip ruby-machine)
eval $(docker-machine env ruby-machine)
docker network create -d overlay multi-host-net
docker-compose -f docker-compose-tarea3-ruby.yml up &

until  wget $(docker-machine ip ruby-machine):3000/public -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Ruby server is not running - sleeping"
  sleep 1
done &&

eval $(docker-machine env nginx-machine)
export NODE3=$(docker-machine ip nginx-machine)
docker-compose -f docker-compose-tarea3-nginx.yml up &
```
+ A continuación, se muestran los pasos a seguir para realizar el despliegue de la aplicación:
```console
$ git clone https://bitbucket.org/tftdp/docker_project
$ cd Tarea3
$ sh tarea3-up.sh
$ curl $(docker-machine ip nginx-machine):8080/public
```
+ Para probar la conexión con la base de datos se accede al contenedor de Ruby:
```console
$ eval $(docker-machine env ruby-machine)
$ docker ps
$ docker exec -it ed16a03dee8d bash
root@ed16a03dee8d:/usr/src/app# rails console
Loading development environment (Rails 4.0.8)
irb(main):001:0> User.first
  User Load (1.0ms)  SELECT "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT 1
=> #<User id: 1, name: "Example User", email: "example@railstutorial.org", created_at: "2017-04-24 16:12:45", updated_at: "2017-04-24 16:12:45", password_digest: "$2a$10$jshXYIy5vQNagWEJcsSMfe23y.oSrvXv.w/RwEyfc89V...", remember_token: "ac60197bf21ccf2b2b60a3d4136a8db94977c24e", admin: true>
```

4: Despliegue remoto de la aplicación, en Amazon Web Services (AWS)
---------------------------------------------------------------------------------------------------------------------

Para desplegar la aplicación en AWS se utilizará el mismo script que en la tarea 3, pero en este caso se crearán las máquinas como instancias de Amazon EC2 utilizando para ello en la creación como driver 'amazonec2'. También se modificará la interfaz por la que se hace el 'cluster-advertise' a eth0 puesto que en estas máquinas no se encuentra la eth1.
Se creará 3 grupos de seguridad en el apartado del EC2 'Security Groups' de la consola de AWS: uno para la máquina de Consul-Postgres (Consul-Postgres), otro para la máquína con Ruby (Ruby) y la última para el servidor web Nginx (Nginx). En todos ellos se añadirá como 'inbound rules' la apertura de puertos ('anywhere'):

+ 22 TCP (SSH)
+ 2376 TCP (Docker)
+ 8500 TCP, 8300 - 8302 TCP, 8301-8302 UDP, 53 UDP (Consul)

Otras reglas 'inbound' a añadir son las siguientes:

+ En el grupo de seguridad de Consul-Postgres se abrirá además el puerto de Postgres, el 5432 TCP. 
+ En el de Ruby se abrirá el puerto 3000 TCP.
+ En el Nginx se abrirá el puerto 8080 TCP.
+ Para el funcionamiento de la network overlay se abrirán en los grupos de seguridad de Ruby y Nginx los puertos 7946 TCP,  4789 UDP y 7946 UDP.

El contenido del script 'Tarea4/tarea4-up.sh' para el arranque de la aplicación es el siguiente:
```console
#!/bin/bash

export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=mysecretpassword
export AWS_ACCESS_KEY_ID=$aws_access_key_id
export AWS_SECRET_ACCESS_KEY=$aws_secret_access_key
export AWS_VPC_ID=vpc-835dbbfa
export AWS_DEFAULT_REGION=us-east-1
export AWS_ZONE=c

export AWS_SECURITY_GROUP=Consul-Postgres
docker-machine -D create --driver amazonec2 \
  --amazonec2-access-key $AWS_ACCESS_KEY_ID \
  --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
  --amazonec2-vpc-id $AWS_VPC_ID \
  --amazonec2-security-group $AWS_SECURITY_GROUP \
  --amazonec2-region $AWS_DEFAULT_REGION \
  --amazonec2-zone $AWS_ZONE \
postgres-machine

eval $(docker-machine env postgres-machine)
export CONSULIP=$(docker-machine ip postgres-machine)
export POSTGRES=$(docker-machine ip postgres-machine)
docker-compose -f docker-compose-tarea4-postgres.yml up &

until PGPASSWORD="$POSTGRES_PASSWORD" psql -h "$POSTGRES" -U "$POSTGRES_USER" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done &&
echo "Postgres available"

export AWS_SECURITY_GROUP=Ruby
docker-machine -D create --driver amazonec2 \
  --amazonec2-access-key $AWS_ACCESS_KEY_ID \
  --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
  --amazonec2-vpc-id $AWS_VPC_ID \
  --amazonec2-security-group $AWS_SECURITY_GROUP \
  --amazonec2-region $AWS_DEFAULT_REGION \
  --amazonec2-zone $AWS_ZONE  \
  --engine-opt="cluster-store=consul://$(docker-machine ip postgres-machine):8500" \
  --engine-opt="cluster-advertise=eth0:2376" \
ruby-machine


export AWS_SECURITY_GROUP=Nginx
docker-machine -D create --driver amazonec2 \
  --amazonec2-access-key $AWS_ACCESS_KEY_ID \
  --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
  --amazonec2-vpc-id $AWS_VPC_ID \
  --amazonec2-security-group $AWS_SECURITY_GROUP \
  --amazonec2-region $AWS_DEFAULT_REGION \
  --amazonec2-zone $AWS_ZONE  \
  --engine-opt="cluster-store=consul://$(docker-machine ip postgres-machine):8500" \
  --engine-opt="cluster-advertise=eth0:2376" \
nginx-machine


eval $(docker-machine env ruby-machine)
export NODE2=$(docker-machine ip ruby-machine)
export CONSULIP=$(docker-machine ip postgres-machine)
docker network create -d overlay --subnet=10.0.9.0/24 multi-host-net
docker-compose -f docker-compose-tarea4-ruby.yml up &

until  wget $(docker-machine ip ruby-machine):3000/public -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Ruby server is not running - sleeping"
  sleep 1
done &&

eval $(docker-machine env nginx-machine)
export NODE3=$(docker-machine ip nginx-machine)
docker-compose -f docker-compose-tarea4-nginx.yml up &
```
El 'AWS_VPC_ID' es el id del Virtual Private Cloud que se puede ver en 'Security Groups' en  la columna 'VPC ID'. El 'AWS_SECURITY_GROUP' es el nombre dado al grupo de seguridad creado en la consola de AWS.

+ Los pasos para desplegar la aplicación en AWS son los siguientes:
```console
$ git clone https://bitbucket.org/tftdp/docker_project
$ cd Tarea4
$ sh tarea4-up.sh
$ curl $(docker-machine ip nginx-machine):8080/public
```
+ Para probar la conexión con la base de datos se accede al contenedor de Ruby:
```console
$ eval $(docker-machine env ruby-machine)
$ docker ps
$ docker exec -it ed16a03dee8d bash
root@ed16a03dee8d:/usr/src/app# rails console
Loading development environment (Rails 4.0.8)
irb(main):001:0> User.first
  User Load (1.0ms)  SELECT "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT 1
=> #<User id: 1, name: "Example User", email: "example@railstutorial.org", created_at: "2017-04-24 16:12:45", updated_at: "2017-04-24 16:12:45", password_digest: "$2a$10$jshXYIy5vQNagWEJcsSMfe23y.oSrvXv.w/RwEyfc89V...", remember_token: "ac60197bf21ccf2b2b60a3d4136a8db94977c24e", admin: true>
```

5: Despliegue de un clúster Swarm local con un maestro y 2 nodos para desplegar la infraestructura. 
-----------------------------------------------------------------------------------------------------
En esta tarea se va a realizar el despliegue de la aplicación en un clúster local, con lo que se utilizarán docker-machine y Swarm para establecer el cluster Swarm en el momento de creación de los nodos.
Se va a tener 4 máquinas virtuales: una con Consul (consul-machine) y las otras tres formarán parte del clúster, una será la máster (nginx-machine) y las otras dos serán esclavos (ruby-machine y postgres-machine).
Para esta tarea se ha separado el fichero docker-compose de Consul-Postgres en dos ficheros, uno para cada servicio y desplegarlo en máquinas diferentes. 
Se ha modificado el script de la tarea 4 para que se creen ahora 4 máquinas y para que las máquinas pertenecientes al clúster se creen con la configuración de Swarm, indicando cuál es el nodo máster y el tipo de 'Swarm discovery', que en este caso utilizará Consul.

En esta tarea se ha incluido además el registro de servicios de los nodos del cluster en Consul por medio de la imagen  'gliderlabs/registrator', que registra y desregistra servicios de todos los contenedores de cada host. En Consul quedará registrada la ip y puerto desde el cual se ofrece el servicio. Para ello, se descargará la imagen 'gliderlabs/registrator' en cada host perteneciente al cluster, antes de realizar el despligue de servicios con docker-compose.

El contenido del script 'Tarea5/tarea5-up.sh' para el despliegue del clúster es el siguiente:

```console
#!/bin/bash

export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=mysecretpassword

docker-machine create -d virtualbox consul-machine
docker-machine regenerate-certs consul-machine -f
eval $(docker-machine env consul-machine)
export CONSULIP=$(docker-machine ip consul-machine)
docker-compose -f docker-compose-tarea5-consul.yml up &

until  wget $(docker-machine ip consul-machine):8500 -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Consul server is not running - sleeping"
  sleep 1
done &&
echo "Consul server available"

docker-machine create -d virtualbox  \
--swarm --swarm-master \
--swarm-discovery="consul://$(docker-machine ip consul-machine):8500" \
--engine-opt="cluster-store=consul://$(docker-machine ip consul-machine):8500" \
--engine-opt="cluster-advertise=eth1:2376" \
nginx-machine

docker-machine regenerate-certs nginx-machine -f

docker-machine create -d virtualbox  \
--swarm \
--swarm-discovery="consul://$(docker-machine ip consul-machine):8500" \
--engine-opt="cluster-store=consul://$(docker-machine ip consul-machine):8500" \
--engine-opt="cluster-advertise=eth1:2376" \
ruby-machine

docker-machine regenerate-certs ruby-machine -f

docker-machine create -d virtualbox  \
--swarm  \
--swarm-discovery="consul://$(docker-machine ip consul-machine):8500" \
--engine-opt="cluster-store=consul://$(docker-machine ip consul-machine):8500" \
--engine-opt="cluster-advertise=eth1:2376" \
postgres-machine

docker-machine regenerate-certs postgres-machine -f

export MANAGER=$(docker-machine ip nginx-machine)
export NODE2=$(docker-machine ip postgres-machine)
export NODE3=$(docker-machine ip ruby-machine)

docker-machine ssh nginx-machine sudo docker network create -d overlay --subnet=10.0.9.0/24 multi-host-net

eval $(docker-machine env nginx-machine)
docker run -d --name=registrator -h $(docker-machine ip nginx-machine) -v=/var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator:v6 consul://$(docker-machine ip consul-machine):8500
eval $(docker-machine env postgres-machine)
docker run -d --name=registrator -h $(docker-machine ip postgres-machine) -v=/var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator:v6 consul://$(docker-machine ip consul-machine):8500
eval $(docker-machine env ruby-machine)
docker run -d --name=registrator -h $(docker-machine ip ruby-machine) -v=/var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator:v6 consul://$(docker-machine ip consul-machine):8500

eval $(docker-machine env postgres-machine)
export POSTGRES=$(docker-machine ip postgres-machine)
docker-compose -f docker-compose-tarea5-postgres.yml up &

until PGPASSWORD="$POSTGRES_PASSWORD" psql -h "$POSTGRES" -U "$POSTGRES_USER" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done &&

eval $(docker-machine env ruby-machine)
docker-compose -f docker-compose-tarea5-ruby.yml up &

until  wget $(docker-machine ip ruby-machine):3000/public -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Ruby server is not running - sleeping"
  sleep 1
done &&

eval $(docker-machine env nginx-machine)
docker-compose -f docker-compose-tarea5-nginx.yml up &

until  wget $(docker-machine ip nginx-machine):8080/public -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Cannot access application from Nginx server"
  sleep 1
done &&

echo "App available from Nginx server"
```
+ Los pasos para desplegar la aplicación en AWS son los siguientes:
```console
$ git clone https://bitbucket.org/tftdp/docker_project
$ cd Tarea5
$ sh tarea5-up.sh
$ curl $(docker-machine ip nginx-machine):8080/public
```
+ Si se ejecuta 'docker-machine ls' se pueden ver los nodos pertenecientes al clúster y cuál es el máster.

+ Para probar la conexión con la base de datos se accede al contenedor de Ruby:
```console
$ eval $(docker-machine env ruby-machine)
$ docker ps
$ docker exec -it ed16a03dee8d bash
root@ed16a03dee8d:/usr/src/app# rails console
Loading development environment (Rails 4.0.8)
irb(main):001:0> User.first
  User Load (1.0ms)  SELECT "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT 1
=> #<User id: 1, name: "Example User", email: "example@railstutorial.org", created_at: "2017-04-24 16:12:45", updated_at: "2017-04-24 16:12:45", password_digest: "$2a$10$jshXYIy5vQNagWEJcsSMfe23y.oSrvXv.w/RwEyfc89V...", remember_token: "ac60197bf21ccf2b2b60a3d4136a8db94977c24e", admin: true>
```
+ Para comprobar el estado del clúster se establecerá el entorno del nodo máster y se ejecutará 'docker info' como se muestra a continuación:
```console
$ eval $(docker-machine env --swarm nginx-machine)
$ docker info
```
+ Se pueden listar los nodos del clúster con el siguiente comando:
```console
$ docker run swarm list consul://$(docker-machine ip consul-machine):8500
```


6: Despliegue de un clúster con un maestro y 2 nodos de manera remota en AWS 
-----------------------------------------------------------------------------------------------------
Para desplegar la aplicación en AWS se utilizará el mismo script que en la tarea 5, pero en este caso se crearán las máquinas como instancias de Amazon EC2 utilizando para ello en la creación como driver 'amazonec2' en lugar de 'virtualbox'. También se modificará la interfaz por la que se hace el 'cluster-advertise' a eth0 puesto que en estas máquinas no se encuentra la eth1.
Se crearán 3 grupos de seguridad en el apartado del EC2 'Security Groups' de la consola de AWS: uno para la máquina de Consul (Security Group con nombre Consul), otro para la máquina con Postgres (Security Group con nombre Postgres) otro para la máquína con Ruby (Security Group con nombre Ruby) y la última para el servidor web Nginx (Security Group con nombre Nginx). En todos ellos se añadirá como 'inbound rules' la apertura de puertos (selección de 'anywhere'):

+ 22 TCP (SSH)
+ 2376 TCP (Docker)
+ 8500 TCP, 8300 - 8302 TCP, 8301-8302 UDP, 53 UDP (Consul)

Otras reglas 'inbound' a añadir son las siguientes:

+ En el grupo de seguridad de Postgres se abrirá además el puerto de Postgres, el 5432 TCP. 
+ En el de Ruby se abrirá el puerto 3000 TCP.
+ En el Nginx (máster) se abrirá el puerto 8080 TCP y además el puerto 3376 TCP, que se abre en el nodo máster para las comunicaciones en el Swarm.
+ Para el funcionamiento de la network overlay se abrirán en los grupos de seguridad de Ruby y Nginx los puertos 7946 TCP,  4789 UDP y 7946 UDP.

El contenido del script 'Tarea6/tarea6-up.sh' para el arranque de la aplicación es el siguiente:
```console
#!/bin/bash

export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=mysecrectpassword
export AWS_ACCESS_KEY_ID=$aws_access_key_id
export AWS_SECRET_ACCESS_KEY=$aws_secret_access_key
export AWS_VPC_ID=vpc-835dbbfa
export AWS_DEFAULT_REGION=us-east-1
export AWS_ZONE=c

export AWS_SECURITY_GROUP=Consul
docker-machine -D create --driver amazonec2 \
  --amazonec2-access-key $AWS_ACCESS_KEY_ID \
  --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
  --amazonec2-vpc-id $AWS_VPC_ID \
  --amazonec2-security-group $AWS_SECURITY_GROUP \
  --amazonec2-region $AWS_DEFAULT_REGION \
  --amazonec2-zone $AWS_ZONE \
consul-machine

export CONSULIP=$(docker-machine ip consul-machine)
eval $(docker-machine env consul-machine)
docker-compose -f docker-compose-tarea6-consul.yml up &

until  wget $(docker-machine ip consul-machine):8500 -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Consul server is not running - sleeping"
  sleep 1
done &&

echo "Consul server available"

export AWS_SECURITY_GROUP=Nginx
docker-machine -D create --driver amazonec2 \
  --swarm --swarm-master \
  --swarm-discovery="consul://$(docker-machine ip consul-machine):8500" \
  --amazonec2-access-key $AWS_ACCESS_KEY_ID \
  --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
  --amazonec2-vpc-id $AWS_VPC_ID \
  --amazonec2-security-group $AWS_SECURITY_GROUP \
  --amazonec2-region $AWS_DEFAULT_REGION \
  --amazonec2-zone $AWS_ZONE \
  --engine-opt="cluster-store=consul://$(docker-machine ip consul-machine):8500" \
  --engine-opt="cluster-advertise=eth0:2376" \
nginx-machine


export AWS_SECURITY_GROUP=Postgres
docker-machine -D create --driver amazonec2 \
  --swarm \
  --swarm-discovery="consul://$(docker-machine ip consul-machine):8500" \
  --amazonec2-access-key $AWS_ACCESS_KEY_ID \
  --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
  --amazonec2-vpc-id $AWS_VPC_ID \
  --amazonec2-security-group $AWS_SECURITY_GROUP \
  --amazonec2-region $AWS_DEFAULT_REGION \
  --amazonec2-zone $AWS_ZONE \
  --engine-opt="cluster-store=consul://$(docker-machine ip consul-machine):8500" \
  --engine-opt="cluster-advertise=eth0:2376" \
postgres-machine


export AWS_SECURITY_GROUP=Ruby
docker-machine -D create --driver amazonec2 \
  --swarm \
  --swarm-discovery="consul://$(docker-machine ip consul-machine):8500" \
  --amazonec2-access-key $AWS_ACCESS_KEY_ID \
  --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
  --amazonec2-vpc-id $AWS_VPC_ID \
  --amazonec2-security-group $AWS_SECURITY_GROUP \
  --amazonec2-region $AWS_DEFAULT_REGION \
  --amazonec2-zone $AWS_ZONE  \
  --engine-opt="cluster-store=consul://$(docker-machine ip consul-machine):8500" \
  --engine-opt="cluster-advertise=eth0:2376" \
ruby-machine


export MANAGER=$(docker-machine ip nginx-machine)
export NODE2=$(docker-machine ip postgres-machine)
export NODE3=$(docker-machine ip ruby-machine)

docker-machine ssh nginx-machine sudo docker network create -d overlay --subnet=10.0.9.0/24 multi-host-net

eval $(docker-machine env nginx-machine)
docker run -d --name=registrator -h $(docker-machine ip nginx-machine) -v=/var/run/docker.sock:/tmp/docker.sock \
gliderlabs/registrator:v6 consul://$(docker-machine ip consul-machine):8500
eval $(docker-machine env postgres-machine)
docker run -d --name=registrator -h $(docker-machine ip postgres-machine) -v=/var/run/docker.sock:/tmp/docker.sock \
gliderlabs/registrator:v6 consul://$(docker-machine ip consul-machine):8500
eval $(docker-machine env ruby-machine)
docker run -d --name=registrator -h $(docker-machine ip ruby-machine) -v=/var/run/docker.sock:/tmp/docker.sock \
gliderlabs/registrator:v6 consul://$(docker-machine ip consul-machine):8500

eval $(docker-machine env postgres-machine)
export POSTGRES=$(docker-machine ip postgres-machine)
docker-compose -f docker-compose-tarea6-postgres.yml up &

until PGPASSWORD="$POSTGRES_PASSWORD" psql -h "$POSTGRES" -U "$POSTGRES_USER" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done &&
echo "Postgres available"

eval $(docker-machine env ruby-machine)
export CONSULIP=$(docker-machine ip consul-machine)
docker-compose -f docker-compose-tarea6-ruby.yml up &

until  wget $(docker-machine ip ruby-machine):3000/public -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Ruby server is not running - sleeping"
  sleep 1
done &&

eval $(docker-machine env nginx-machine)
docker-compose -f docker-compose-tarea6-nginx.yml up &

until  wget $(docker-machine ip nginx-machine):8080/public -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Cannot access application from Nginx server"
  sleep 1
done &&

echo "App available from Nginx server"
```
El 'AWS_VPC_ID' es el id del Virtual Private Cloud que se puede ver en 'Security Groups' en  la columna 'VPC ID'. El 'AWS_SECURITY_GROUP' es el nombre dado al grupo de seguridad creado en la consola de AWS.

+ Los pasos para desplegar la aplicación en AWS son los siguientes:
```console
$ git clone https://bitbucket.org/tftdp/docker_project
$ cd Tarea6
$ sh tarea6-up.sh
$ curl $(docker-machine ip nginx-machine):8080/public
```
+ Si se ejecuta 'docker-machine ls' se pueden ver los nodos pertenecientes al clúster y cuál es el máster.

+ Para probar la conexión con la base de datos se accede al contenedor de Ruby:
```console
$ eval $(docker-machine env ruby-machine)
$ docker ps
$ docker exec -it ed16a03dee8d bash
root@ed16a03dee8d:/usr/src/app# rails console
Loading development environment (Rails 4.0.8)
irb(main):001:0> User.first
  User Load (1.0ms)  SELECT "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT 1
=> #<User id: 1, name: "Example User", email: "example@railstutorial.org", created_at: "2017-04-24 16:12:45", updated_at: "2017-04-24 16:12:45", password_digest: "$2a$10$jshXYIy5vQNagWEJcsSMfe23y.oSrvXv.w/RwEyfc89V...", remember_token: "ac60197bf21ccf2b2b60a3d4136a8db94977c24e", admin: true>
```
+ Para comprobar el estado del clúster se establecerá el entorno del nodo máster y se ejecutará 'docker info' como se muestra a continuación:
```console
$ eval $(docker-machine env --swarm nginx-machine)
$ docker info
```
+ Se pueden listar los nodos del clúster con el siguiente comando:
```console
$ docker run swarm list consul://$(docker-machine ip consul-machine):8500
```

#!/bin/bash

export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=mysecretpassword

docker-machine create -d virtualbox postgres-machine
docker-machine regenerate-certs postgres-machine -f
eval $(docker-machine env postgres-machine)
export CONSULIP=$(docker-machine ip postgres-machine)
export POSTGRES=$(docker-machine ip postgres-machine)
docker-compose -f docker-compose-tarea3-postgres.yml up &

until PGPASSWORD="$POSTGRES_PASSWORD" psql -h "$POSTGRES" -U "$POSTGRES_USER" -c '\l'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done &&

docker-machine create -d virtualbox  \
--engine-opt="cluster-store=consul://$(docker-machine ip postgres-machine):8500" \
--engine-opt="cluster-advertise=eth1:2376" \
ruby-machine

docker-machine regenerate-certs ruby-machine -f

docker-machine create -d virtualbox  \
--engine-opt="cluster-store=consul://$(docker-machine ip postgres-machine):8500" \
--engine-opt="cluster-advertise=eth1:2376" \
nginx-machine

docker-machine regenerate-certs nginx-machine -f

export NODE2=$(docker-machine ip ruby-machine)
eval $(docker-machine env ruby-machine)
docker network create -d overlay multi-host-net
docker-compose -f docker-compose-tarea3-ruby.yml up &

until  wget $(docker-machine ip ruby-machine):3000/public -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Ruby server is not running - sleeping"
  sleep 1
done &&

eval $(docker-machine env nginx-machine)
export NODE3=$(docker-machine ip nginx-machine)
docker-compose -f docker-compose-tarea3-nginx.yml up &

#!/bin/bash

export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=mysecrectpassword
export AWS_ACCESS_KEY_ID=$aws_access_key_id
export AWS_SECRET_ACCESS_KEY=$aws_secret_access_key
export AWS_VPC_ID=vpc-835dbbfa
export AWS_DEFAULT_REGION=us-east-1
export AWS_ZONE=c

export AWS_SECURITY_GROUP=Consul
docker-machine -D create --driver amazonec2 \
  --amazonec2-access-key $AWS_ACCESS_KEY_ID \
  --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
  --amazonec2-vpc-id $AWS_VPC_ID \
  --amazonec2-security-group $AWS_SECURITY_GROUP \
  --amazonec2-region $AWS_DEFAULT_REGION \
  --amazonec2-zone $AWS_ZONE \
consul-machine

export CONSULIP=$(docker-machine ip consul-machine)
eval $(docker-machine env consul-machine)
docker-compose -f docker-compose-tarea6-consul.yml up &

until  wget $(docker-machine ip consul-machine):8500 -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Consul server is not running - sleeping"
  sleep 1
done &&

echo "Consul server available"

export AWS_SECURITY_GROUP=Nginx
docker-machine -D create --driver amazonec2 \
  --swarm --swarm-master \
  --swarm-discovery="consul://$(docker-machine ip consul-machine):8500" \
  --amazonec2-access-key $AWS_ACCESS_KEY_ID \
  --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
  --amazonec2-vpc-id $AWS_VPC_ID \
  --amazonec2-security-group $AWS_SECURITY_GROUP \
  --amazonec2-region $AWS_DEFAULT_REGION \
  --amazonec2-zone $AWS_ZONE \
  --engine-opt="cluster-store=consul://$(docker-machine ip consul-machine):8500" \
  --engine-opt="cluster-advertise=eth0:2376" \
nginx-machine


export AWS_SECURITY_GROUP=Postgres
docker-machine -D create --driver amazonec2 \
  --swarm \
  --swarm-discovery="consul://$(docker-machine ip consul-machine):8500" \
  --amazonec2-access-key $AWS_ACCESS_KEY_ID \
  --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
  --amazonec2-vpc-id $AWS_VPC_ID \
  --amazonec2-security-group $AWS_SECURITY_GROUP \
  --amazonec2-region $AWS_DEFAULT_REGION \
  --amazonec2-zone $AWS_ZONE \
  --engine-opt="cluster-store=consul://$(docker-machine ip consul-machine):8500" \
  --engine-opt="cluster-advertise=eth0:2376" \
postgres-machine


export AWS_SECURITY_GROUP=Ruby
docker-machine -D create --driver amazonec2 \
  --swarm \
  --swarm-discovery="consul://$(docker-machine ip consul-machine):8500" \
  --amazonec2-access-key $AWS_ACCESS_KEY_ID \
  --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
  --amazonec2-vpc-id $AWS_VPC_ID \
  --amazonec2-security-group $AWS_SECURITY_GROUP \
  --amazonec2-region $AWS_DEFAULT_REGION \
  --amazonec2-zone $AWS_ZONE  \
  --engine-opt="cluster-store=consul://$(docker-machine ip consul-machine):8500" \
  --engine-opt="cluster-advertise=eth0:2376" \
ruby-machine


export MANAGER=$(docker-machine ip nginx-machine)
export NODE2=$(docker-machine ip postgres-machine)
export NODE3=$(docker-machine ip ruby-machine)

docker-machine ssh nginx-machine sudo docker network create -d overlay --subnet=10.0.9.0/24 multi-host-net

eval $(docker-machine env nginx-machine)
docker run -d --name=registrator -h $(docker-machine ip nginx-machine) -v=/var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator:v6 consul://$(docker-machine ip consul-machine):8500
eval $(docker-machine env postgres-machine)
docker run -d --name=registrator -h $(docker-machine ip postgres-machine) -v=/var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator:v6 consul://$(docker-machine ip consul-machine):8500
eval $(docker-machine env ruby-machine)
docker run -d --name=registrator -h $(docker-machine ip ruby-machine) -v=/var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator:v6 consul://$(docker-machine ip consul-machine):8500

eval $(docker-machine env postgres-machine)
export POSTGRES=$(docker-machine ip postgres-machine)
docker-compose -f docker-compose-tarea6-postgres.yml up &

until PGPASSWORD="$POSTGRES_PASSWORD" psql -h "$POSTGRES" -U "$POSTGRES_USER" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done &&
echo "Postgres available"

eval $(docker-machine env ruby-machine)
export CONSULIP=$(docker-machine ip consul-machine)
docker-compose -f docker-compose-tarea6-ruby.yml up &

until  wget $(docker-machine ip ruby-machine):3000/public -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Ruby server is not running - sleeping"
  sleep 1
done &&

eval $(docker-machine env nginx-machine)
docker-compose -f docker-compose-tarea6-nginx.yml up &

until  wget $(docker-machine ip nginx-machine):8080/public -O /dev/null -S --quiet 2>&1 | grep '200 OK' ; do
  >&2 echo "Cannot access application from Nginx server"
  sleep 1
done &&

echo "App available from Nginx server"
